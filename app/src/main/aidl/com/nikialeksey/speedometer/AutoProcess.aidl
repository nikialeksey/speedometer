package com.nikialeksey.speedometer;

interface AutoProcess {
    long speed();
    long rotation();
}