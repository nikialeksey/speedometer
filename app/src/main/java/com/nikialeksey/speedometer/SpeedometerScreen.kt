package com.nikialeksey.speedometer

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.constraintlayout.motion.widget.MotionScene
import kotlinx.android.synthetic.main.activity_main.view.*

class SpeedometerScreen : MotionLayout, Speedometer {

    private val motionScene: MotionScene by lazy {
        MotionLayout::class.java
            .getDeclaredField("mScene")
            .apply { isAccessible = true }
            .get(this) as MotionScene
    }
    private val transitionList: List<MotionScene.Transition> by lazy {
        MotionScene::class.java
            .getDeclaredField("mTransitionList")
            .apply { isAccessible = true }
            .get(motionScene) as List<MotionScene.Transition>
    }

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        // we need to change translation in the scene for adaptive transitions when different
        // speedometer screen width
        getConstraintSet(R.id.speedometerMain).apply {
            getConstraint(R.id.tachometerScale).transform.translationX = measuredWidth / 5f
        }
        getConstraintSet(R.id.tachometerMain).apply {
            getConstraint(R.id.speedometerScale).transform.translationX = - measuredWidth / 5f
        }
        // and in the key frames too
        transitionList[0].keyFrameList[0]
            .getKeyFramesForView(
                tachometerScale.id
            )[0].setValue("translationX", measuredWidth / 1.5f)
        transitionList[0].keyFrameList[0]
            .getKeyFramesForView(
                speedometerScale.id
            )[0].setValue("translationX", -measuredWidth / 1.5f)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return if (event.action == MotionEvent.ACTION_MOVE && event.pointerCount != 2) {
            false
        } else {
            super.onTouchEvent(event)
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        tachometerScale.start()
        speedometerScale.start()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        tachometerScale.stop()
        speedometerScale.stop()
    }

    override fun value(speed: () -> Long, rotation: () -> Long) {
        this.speedometerScale.value(speed)
        this.tachometerScale.value(rotation)
    }

    companion object {
        private const val TAG = "SpeedometerScreen"
    }
}