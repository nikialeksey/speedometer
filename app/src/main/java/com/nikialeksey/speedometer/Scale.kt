package com.nikialeksey.speedometer

interface Scale {
    fun value(value: () -> Long)
    fun start()
    fun stop()
}