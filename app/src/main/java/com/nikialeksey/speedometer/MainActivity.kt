package com.nikialeksey.speedometer

import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.os.Process
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : Activity() {

    private var autoProcess: AutoProcess? = null

    private val connection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            autoProcess = AutoProcess.Stub.asInterface(service)
            Log.d(TAG, "AutoProcess connected!")
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            autoProcess = null
            Log.d(TAG, "AutoProcess disconnected!")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        speedometerLayout.keepScreenOn = true

        Log.d(TAG, "Created. Pid: ${Process.myPid()}")
    }

    override fun onStart() {
        super.onStart()
        speedometerLayout.value({
            autoProcess?.speed() ?: 0L
        }, {
            autoProcess?.rotation() ?: 0L
        })
        Intent(this, AutoService::class.java).also { intent ->
            bindService(intent, connection, Context.BIND_AUTO_CREATE)
        }
    }

    override fun onStop() {
        super.onStop()
        speedometerLayout.value({ 0L }, { 0L })
        unbindService(connection)
    }

    companion object {
        private const val TAG = "MainActivity"
    }
}