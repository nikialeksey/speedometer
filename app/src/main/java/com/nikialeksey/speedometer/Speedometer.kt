package com.nikialeksey.speedometer

interface Speedometer {
    fun value(speed: () -> Long, rotation: () -> Long)
}