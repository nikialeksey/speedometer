package com.nikialeksey.speedometer

import android.app.Service
import android.content.Intent
import android.os.*
import android.util.Log
import java.util.*
import java.util.concurrent.atomic.AtomicLong
import kotlin.math.sin

class AutoService : Service() {

    private val speed = AtomicLong()
    private val rotation = AtomicLong()

    private val random = Random()
    private val ticksThread = HandlerThread("auto").apply { start() }
    private val ticks = Handler(ticksThread.looper)
    private var time: Long = 0
    private val tick: Runnable = object : Runnable {
        override fun run() {
            time = SystemClock.elapsedRealtime()

            speed.set(Math.abs(150 * sin(0.01 * time) + 50 * sin(0.02 * time)).toLong())
            rotation.set(Math.abs(50 * sin(0.02 * time) + 150 * sin(0.09 * time)).toLong())

            val delay: Long = 20 + (1800 * random.nextDouble()).toLong()
            ticks.postDelayed(this, delay)
        }
    }

    private val binder = object : AutoProcess.Stub() {
        override fun speed(): Long {
            return speed.get()
        }

        override fun rotation(): Long {
            return rotation.get()
        }
    }

    override fun onCreate() {
        super.onCreate()
        start()
        Log.d(TAG, "Started. Pid: ${Process.myPid()}")
    }

    override fun onBind(intent: Intent): IBinder {
        return binder
    }

    override fun onDestroy() {
        super.onDestroy()
        stop()
        Log.d(TAG, "Destroyed.")
    }

    private fun start() {
        ticks.removeCallbacks(tick)
        ticks.post(tick)
    }

    private fun stop() {
        ticks.removeCallbacks(tick)
    }

    companion object {
        private const val TAG = "AutoService"
    }
}