package com.nikialeksey.speedometer

import android.content.Context
import android.graphics.*
import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import android.util.AttributeSet
import android.view.View
import java.util.concurrent.TimeUnit
import kotlin.math.*

class ScaleView : View, Scale {

    private var scaleRadius: Int = 0
    private var scaleWidth: Int = 0
    private var scaleCenterX: Int = 0
    private var scaleCenterY: Int = 0

    private var markLength: Int = 0
    private var markWidth: Int = 0
    private var marksCount: Int = 0
    private var marksColor: Int = Color.BLACK
    private var marks = floatArrayOf() // [x0 y0 x1 y1], [x2 y2 x3 y3], ...
    private var markFontSize = 0f
    private var marksTypeface = Typeface.create("sans", Typeface.NORMAL)
    private var marksTextPaths: Array<Path> = arrayOf()
    private var marksText: Array<String> = arrayOf()

    private var scaleValueMin: Int = 0
    private var scaleValueMax: Int = 0
    private var arrowWidth: Int = 0
    private var arrowLength: Int = 0
    private var currentArrowValue: Double = scaleValueMax.toDouble()
    private var scaleValue: () -> Long = { currentArrowValue.toLong() }

    private val marksPaint = Paint()
    private val arrowPaint = Paint()

    private val ticks = Handler(Looper.getMainLooper())
    private val tick: Runnable = object : Runnable {
        override fun run() {
            invalidate()
            ticks.postDelayed(this, TICK_DELAY_MS)
        }
    }
    private var onDrawTime: Long = 0

    constructor(context: Context?) : super(context) {
        init(null)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {
        val defaultMarksCount = resources.getInteger(R.integer.defaultMarksCount)
        val defaultMarkWidth = resources.getDimensionPixelSize(R.dimen.defaultMarkWidth)
        val defaultMarkLength = resources.getDimensionPixelSize(R.dimen.defaultMarkLength)
        val defaultMarksColor = Color.BLACK
        val defaultMarksTypeface = Typeface.NORMAL

        val defaultScaleValueMin = resources.getInteger(R.integer.defaultScaleValueMin)
        val defaultScaleValueMax = resources.getInteger(R.integer.defaultScaleValueMax)
        val defaultArrowLength = resources.getDimensionPixelSize(R.dimen.defaultArrowLength)
        val defaultArrowWidth = resources.getDimensionPixelSize(R.dimen.defaultArrowWidth)

        markFontSize = resources.getDimension(R.dimen.defaultMarkFontSize)

        if (attrs != null) {
            val array = context.obtainStyledAttributes(attrs, R.styleable.ScaleView)

            marksCount = array.getInteger(R.styleable.ScaleView_marksCount, defaultMarksCount)
            markWidth = array.getDimensionPixelSize(R.styleable.ScaleView_markWidth, defaultMarkWidth)
            markLength = array.getDimensionPixelSize(R.styleable.ScaleView_markLength, defaultMarkLength)
            marksColor = array.getColor(R.styleable.ScaleView_marksColor, defaultMarksColor)
            marksTypeface = Typeface.create("sans", array.getInteger(R.styleable.ScaleView_marksStyle, defaultMarksTypeface))

            scaleValueMin = array.getInteger(R.styleable.ScaleView_scaleValueMin, defaultScaleValueMin)
            scaleValueMax = array.getInteger(R.styleable.ScaleView_scaleValueMax, defaultScaleValueMax)
            arrowLength = array.getDimensionPixelSize(R.styleable.ScaleView_arrowLength, defaultArrowLength)
            arrowWidth = array.getDimensionPixelSize(R.styleable.ScaleView_arrowWidth, defaultArrowWidth)

            array.recycle()
        } else {
            marksCount = defaultMarksCount
            markWidth = defaultMarkWidth
            markLength = defaultMarkLength
            marksColor = defaultMarksColor
            marksTypeface = Typeface.create("sans", defaultMarksTypeface)

            scaleValueMin = defaultScaleValueMin
            scaleValueMax = defaultScaleValueMax
            arrowLength = defaultArrowLength
            arrowWidth = defaultArrowWidth
        }

        currentArrowValue = scaleValueMin.toDouble()
        scaleValue = { currentArrowValue.toLong() }

        marks = FloatArray(marksCount * 4)
        marksTextPaths = Array(marksCount) { Path() }
        marksText = Array(marksCount) {
            "${scaleValueMin + it * (scaleValueMax - scaleValueMin) / (marksCount - 1)}"
        }
        marksPaint.strokeWidth = markWidth.toFloat()
        marksPaint.color = marksColor
        marksPaint.typeface = marksTypeface
        marksPaint.textSize = markFontSize

        arrowPaint.strokeWidth = arrowWidth.toFloat()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        if (measuredHeight > measuredWidth) { // portrait
            scaleWidth = measuredWidth - paddingStart - paddingEnd
            scaleRadius = scaleWidth / 2
        } else { // landscape
            scaleRadius = min(
                measuredHeight - paddingTop - paddingBottom,
                measuredWidth / 2 - paddingStart - paddingEnd
            )
            scaleWidth = scaleRadius * 2
        }

        scaleCenterX = measuredWidth / 2
        scaleCenterY = measuredHeight - (measuredHeight - scaleRadius) / 2

        for (mark in 0 until marksCount) {
            // PI .. 0
            val markAlpha: Float =
                ((Math.PI / (marksCount - 1)) * (marksCount - 1 - mark)).toFloat()
            val cosAlpha = cos(markAlpha)
            val sinAlpha = sin(markAlpha)
            val markX0 = scaleRadius * cosAlpha
            val markY0 = scaleRadius * sinAlpha
            val markX1 = (scaleRadius - markLength) * cosAlpha
            val markY1 = (scaleRadius - markLength) * sinAlpha

            marks[mark * 4] = markX0 + scaleCenterX
            marks[mark * 4 + 1] = scaleCenterY - markY0
            marks[mark * 4 + 2] = markX1 + scaleCenterX
            marks[mark * 4 + 3] = scaleCenterY - markY1

            marksTextPaths[mark].reset()
            val startAngle = Math.toDegrees(2 * Math.PI - markAlpha) -
                    Math.toDegrees(asin(marksPaint.measureText(marksText[mark]) / (2.0 * scaleRadius)))
            val sweepAngle = Math.toDegrees(Math.PI)
            marksTextPaths[mark].addArc(
                (scaleCenterX - scaleRadius).toFloat() + markLength,
                (scaleCenterY - scaleRadius).toFloat() + markLength,
                (scaleCenterX + scaleRadius).toFloat() - markLength,
                scaleCenterY.toFloat() + scaleRadius - markLength,
                startAngle.toFloat(),
                sweepAngle.toFloat()
            )
        }
    }

    override fun onDraw(canvas: Canvas) {
        val paintAlpha = (alpha * 255).toInt()
        // draw marks
        marksPaint.alpha = paintAlpha
        canvas.drawLines(marks, marksPaint)
        marksTextPaths.forEachIndexed { mark, path ->
            val text = marksText[mark]
            canvas.drawTextOnPath(text, path, 0f, markFontSize, marksPaint)
        }

        // draw arrow
        arrowPaint.alpha = paintAlpha
        val arrowX0 = scaleCenterX.toFloat()
        val arrowY0 = scaleCenterY.toFloat()
        // arrowValue
        val currentScaleValue = scaleValue()
        val currentTime: Long = SystemClock.elapsedRealtime()
        val direction = if (currentScaleValue - currentArrowValue < 0) -1 else 1
        val possibleArrowWay: Double = ARROW_SPEED * (currentTime - onDrawTime)
        val necessaryArrowWay: Double = abs(currentScaleValue - currentArrowValue)
        currentArrowValue += direction * min(possibleArrowWay, necessaryArrowWay)
        // PI - scaleValueMin, 0 - scaleValueMax
        val arrowAlpha = (Math.PI - Math.PI * (currentArrowValue - scaleValueMin) / (scaleValueMax - scaleValueMin)).toFloat()
        val arrowX1 = arrowLength * cos(arrowAlpha) + scaleCenterX
        val arrowY1 = scaleCenterY - arrowLength * sin(arrowAlpha)
        canvas.drawLine(arrowX0, arrowY0, arrowX1, arrowY1, arrowPaint)

        onDrawTime = currentTime
    }

    override fun value(value: () -> Long) {
        this.scaleValue = value
    }

    override fun start() {
        ticks.removeCallbacks(tick)
        ticks.post(tick)
    }

    override fun stop() {
        ticks.removeCallbacks(tick)
    }

    companion object {
        private const val ARROW_SPEED = 0.1
        private const val FPS = 60
        private val TICK_DELAY_MS = TimeUnit.SECONDS.toMillis(1) / FPS
    }
}